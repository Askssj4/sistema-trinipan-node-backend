const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('catalogo_producto', {
    ID_PRODUCTO: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_CATEGORIA: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'categoria',
        key: 'ID_CATEGORIA'
      }
    },
    ID_TIPO_PRODUCTO: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tipo_producto',
        key: 'ID_TIPO_PRODUCTO'
      }
    },
    ID_PROVEEDOR: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'proveedor',
        key: 'ID_PROVEEDOR'
      }
    },
    ID_UNIDAD_DE_MEDIDA: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'unidad_de_medida',
        key: 'ID_UNIDAD_DE_MEDIDA'
      }
    },
    NOMBRE_PRODUCTO: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    TIPO_PRODUCTO: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    DESCRIPCION: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    ES_LOCAL: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'CATALOGO_PRODUCTO',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_PRODUCTO" },
        ]
      },
      {
        name: "FK_AGRUPA",
        using: "BTREE",
        fields: [
          { name: "ID_CATEGORIA" },
        ]
      },
      {
        name: "FK_PERTENECE",
        using: "BTREE",
        fields: [
          { name: "ID_TIPO_PRODUCTO" },
        ]
      },
      {
        name: "FK_TIENE",
        using: "BTREE",
        fields: [
          { name: "ID_UNIDAD_DE_MEDIDA" },
        ]
      },
      {
        name: "FK_ABASTECE",
        using: "BTREE",
        fields: [
          { name: "ID_PROVEEDOR" },
        ]
      },
    ]
  });
};
