const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('categoria', {
    ID_CATEGORIA: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_TIPO_PRODUCTO: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tipo_producto',
        key: 'ID_TIPO_PRODUCTO'
      }
    },
    NOMBRE_CATEGORIA: {
      type: DataTypes.STRING(30),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'CATEGORIA',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_CATEGORIA" },
        ]
      },
      {
        name: "FK_ORDENA",
        using: "BTREE",
        fields: [
          { name: "ID_TIPO_PRODUCTO" },
        ]
      },
    ]
  });
};
