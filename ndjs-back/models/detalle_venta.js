const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('detalle_venta', {
    ID_DETALLA_VENTA: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_PRODUCTO: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'catalogo_productos',
        key: 'ID_PRODUCTO'
      }
    },
    ID_VENTA: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'ventas',
        key: 'ID_VENTA'
      }
    },
    CANTIDAD_VENTA: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    PRECIO_VENTA: {
      type: DataTypes.DECIMAL(10,0),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'detalle_venta',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_DETALLA_VENTA" },
        ]
      },
      {
        name: "FK_RELATIONSHIP_13",
        using: "BTREE",
        fields: [
          { name: "ID_PRODUCTO" },
        ]
      },
      {
        name: "FK_RELATIONSHIP_14",
        using: "BTREE",
        fields: [
          { name: "ID_VENTA" },
        ]
      },
    ]
  });
};
