const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tipo_categoria_producto_inventario', {
    ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_TIPO_PRODUCTO: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'tipo_producto',
        key: 'ID_TIPO_PRODUCTO'
      }
    },
    ID_CATEGORIA: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'categoria_producto',
        key: 'ID_CATEGORIA'
      }
    },
    NOMBRE_TIPO_CATEGORIA_INVENTARIO: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'tipo_categoria_producto_inventario',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO" },
        ]
      },
      {
        name: "FK_RELATIONSHIP_8",
        using: "BTREE",
        fields: [
          { name: "ID_TIPO_PRODUCTO" },
        ]
      },
      {
        name: "FK_RELATIONSHIP_9",
        using: "BTREE",
        fields: [
          { name: "ID_CATEGORIA" },
        ]
      },
    ]
  });
};
