var DataTypes = require("sequelize").DataTypes;
var _catalogo_productos = require("./catalogo_productos");
var _categoria_producto = require("./categoria_producto");
var _detalle_pedido = require("./detalle_pedido");
var _detalle_pedido_inventario = require("./detalle_pedido_inventario");
var _detalle_venta = require("./detalle_venta");
var _detalle_venta_inventario = require("./detalle_venta_inventario");
var _inventario = require("./inventario");
var _pedido = require("./pedido");
var _proveedor = require("./proveedor");
var _rol = require("./rol");
var _tipo_categoria_producto_inventario = require("./tipo_categoria_producto_inventario");
var _tipo_producto = require("./tipo_producto");
var _unidades_de_medida = require("./unidades_de_medida");
var _usuario = require("./usuario");
var _ventas = require("./ventas");

function initModels(sequelize) {
  var catalogo_productos = _catalogo_productos(sequelize, DataTypes);
  var categoria_producto = _categoria_producto(sequelize, DataTypes);
  var detalle_pedido = _detalle_pedido(sequelize, DataTypes);
  var detalle_pedido_inventario = _detalle_pedido_inventario(sequelize, DataTypes);
  var detalle_venta = _detalle_venta(sequelize, DataTypes);
  var detalle_venta_inventario = _detalle_venta_inventario(sequelize, DataTypes);
  var inventario = _inventario(sequelize, DataTypes);
  var pedido = _pedido(sequelize, DataTypes);
  var proveedor = _proveedor(sequelize, DataTypes);
  var rol = _rol(sequelize, DataTypes);
  var tipo_categoria_producto_inventario = _tipo_categoria_producto_inventario(sequelize, DataTypes);
  var tipo_producto = _tipo_producto(sequelize, DataTypes);
  var unidades_de_medida = _unidades_de_medida(sequelize, DataTypes);
  var usuario = _usuario(sequelize, DataTypes);
  var ventas = _ventas(sequelize, DataTypes);

  detalle_pedido.belongsTo(catalogo_productos, { as: "ID_PRODUCTO_catalogo_producto", foreignKey: "ID_PRODUCTO"});
  catalogo_productos.hasMany(detalle_pedido, { as: "detalle_pedidos", foreignKey: "ID_PRODUCTO"});
  detalle_venta.belongsTo(catalogo_productos, { as: "ID_PRODUCTO_catalogo_producto", foreignKey: "ID_PRODUCTO"});
  catalogo_productos.hasMany(detalle_venta, { as: "detalle_venta", foreignKey: "ID_PRODUCTO"});
  catalogo_productos.belongsTo(categoria_producto, { as: "ID_CATEGORIA_categoria_producto", foreignKey: "ID_CATEGORIA"});
  categoria_producto.hasMany(catalogo_productos, { as: "catalogo_productos", foreignKey: "ID_CATEGORIA"});
  tipo_categoria_producto_inventario.belongsTo(categoria_producto, { as: "ID_CATEGORIA_categoria_producto", foreignKey: "ID_CATEGORIA"});
  categoria_producto.hasMany(tipo_categoria_producto_inventario, { as: "tipo_categoria_producto_inventarios", foreignKey: "ID_CATEGORIA"});
  detalle_pedido_inventario.belongsTo(detalle_pedido, { as: "ID_DETALLE_PEDIDO_detalle_pedido", foreignKey: "ID_DETALLE_PEDIDO"});
  detalle_pedido.hasMany(detalle_pedido_inventario, { as: "detalle_pedido_inventarios", foreignKey: "ID_DETALLE_PEDIDO"});
  detalle_venta_inventario.belongsTo(detalle_venta, { as: "ID_DETALLA_VENTA_detalle_ventum", foreignKey: "ID_DETALLA_VENTA"});
  detalle_venta.hasMany(detalle_venta_inventario, { as: "detalle_venta_inventarios", foreignKey: "ID_DETALLA_VENTA"});
  detalle_pedido_inventario.belongsTo(inventario, { as: "ID_INVENTARIO_inventario", foreignKey: "ID_INVENTARIO"});
  inventario.hasMany(detalle_pedido_inventario, { as: "detalle_pedido_inventarios", foreignKey: "ID_INVENTARIO"});
  detalle_venta_inventario.belongsTo(inventario, { as: "ID_INVENTARIO_inventario", foreignKey: "ID_INVENTARIO"});
  inventario.hasMany(detalle_venta_inventario, { as: "detalle_venta_inventarios", foreignKey: "ID_INVENTARIO"});
  detalle_pedido.belongsTo(pedido, { as: "ID_PEDIDO_pedido", foreignKey: "ID_PEDIDO"});
  pedido.hasMany(detalle_pedido, { as: "detalle_pedidos", foreignKey: "ID_PEDIDO"});
  catalogo_productos.belongsTo(proveedor, { as: "ID_PROVEEDOR_proveedor", foreignKey: "ID_PROVEEDOR"});
  proveedor.hasMany(catalogo_productos, { as: "catalogo_productos", foreignKey: "ID_PROVEEDOR"});
  pedido.belongsTo(proveedor, { as: "ID_PROVEEDOR_proveedor", foreignKey: "ID_PROVEEDOR"});
  proveedor.hasMany(pedido, { as: "pedidos", foreignKey: "ID_PROVEEDOR"});
  usuario.belongsTo(rol, { as: "ID_ROL_rol", foreignKey: "ID_ROL"});
  rol.hasMany(usuario, { as: "usuarios", foreignKey: "ID_ROL"});
  inventario.belongsTo(tipo_categoria_producto_inventario, { as: "ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO_tipo_categoria_producto_inventario", foreignKey: "ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO"});
  tipo_categoria_producto_inventario.hasMany(inventario, { as: "inventarios", foreignKey: "ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO"});
  catalogo_productos.belongsTo(tipo_producto, { as: "ID_TIPO_PRODUCTO_tipo_producto", foreignKey: "ID_TIPO_PRODUCTO"});
  tipo_producto.hasMany(catalogo_productos, { as: "catalogo_productos", foreignKey: "ID_TIPO_PRODUCTO"});
  tipo_categoria_producto_inventario.belongsTo(tipo_producto, { as: "ID_TIPO_PRODUCTO_tipo_producto", foreignKey: "ID_TIPO_PRODUCTO"});
  tipo_producto.hasMany(tipo_categoria_producto_inventario, { as: "tipo_categoria_producto_inventarios", foreignKey: "ID_TIPO_PRODUCTO"});
  catalogo_productos.belongsTo(unidades_de_medida, { as: "ID_MEDIDA_unidades_de_medida", foreignKey: "ID_MEDIDA"});
  unidades_de_medida.hasMany(catalogo_productos, { as: "catalogo_productos", foreignKey: "ID_MEDIDA"});
  detalle_venta.belongsTo(ventas, { as: "ID_VENTA_venta", foreignKey: "ID_VENTA"});
  ventas.hasMany(detalle_venta, { as: "detalle_venta", foreignKey: "ID_VENTA"});

  return {
    catalogo_productos,
    categoria_producto,
    detalle_pedido,
    detalle_pedido_inventario,
    detalle_venta,
    detalle_venta_inventario,
    inventario,
    pedido,
    proveedor,
    rol,
    tipo_categoria_producto_inventario,
    tipo_producto,
    unidades_de_medida,
    usuario,
    ventas,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
