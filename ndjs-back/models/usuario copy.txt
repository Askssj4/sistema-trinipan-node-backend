const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');

module.exports = function(sequelize, DataTypes) {
  const usuario = sequelize.define('usuario', {
    ID_USUARIO: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    USERNAME: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    PASSWORD: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ACTIVO: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    FECHA_CREACION: {
      type: DataTypes.DATE,
      allowNull: true
    },
    FECHA_MODIFICACION: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'usuario',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_USUARIO" },
        ]
      },
    ]
  });

  const generateHash = async(usuarioo) =>{
    // const salt = await bcrypt.genSalt(8); 
    // usuario.CONTRASENIA_USUARIO = await bcrypt.hash(usuario.CONTRASENIA_USUARIO, salt);
    // console.log("hook called");
    // console.log("la contrasenia a hashear: ", usuario.CONTRASENIA_USUARIO)
  
    //const salt = bcrypt.genSalt(10);
    usuarioo.PASSWORD = usuarioo.PASSWORD && usuarioo.PASSWORD != "" ? bcrypt.hashSync(usuarioo.PASSWORD, 12) : "";
  
  }

  usuario.addHook('beforeCreate',generateHash);
  //beforeCreate: generateHash

  usuario.beforeUpdate(
    async (usuarioo) => await generateHash(usuarioo)
  )

    
  usuario.prototype.validPassword=function(password) {
    let hash = bcrypt.hash(password, 12, function(err, hash){
      if(err){
        console.log('error', err)
      }
      else{
        // console.log("el hash: ", hash);
        return hash;
      }
    });
    // console.log( "el hash: ", hash);
    return bcrypt.compareSync(password, this.PASSWORD);
  }        
  
 
  return usuario;

};

