const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('detalle_venta_inventario', {
    ID_DETALLE_VENTA_INVENTARIO: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_DETALLA_VENTA: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'detalle_venta',
        key: 'ID_DETALLA_VENTA'
      }
    },
    ID_INVENTARIO: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'inventario',
        key: 'ID_INVENTARIO'
      }
    },
    CANTIDAD_ACTUAL: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    CANTIDAD_VENTA: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'detalle_venta_inventario',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_DETALLE_VENTA_INVENTARIO" },
        ]
      },
      {
        name: "FK_RELATIONSHIP_15",
        using: "BTREE",
        fields: [
          { name: "ID_DETALLA_VENTA" },
        ]
      },
      {
        name: "FK_RELATIONSHIP_16",
        using: "BTREE",
        fields: [
          { name: "ID_INVENTARIO" },
        ]
      },
    ]
  });
};
