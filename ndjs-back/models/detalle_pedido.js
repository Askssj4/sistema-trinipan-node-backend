const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('detalle_pedido', {
    ID_DETALLE_PEDIDO: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_PEDIDO: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'pedido',
        key: 'ID_PEDIDO'
      }
    },
    ID_PRODUCTO: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'catalogo_productos',
        key: 'ID_PRODUCTO'
      }
    },
    FECHA_VENCIMIENTO: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    CANTIDAD_PEDIDO: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    PRECIO_COMPRA: {
      type: DataTypes.DECIMAL(10,0),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'detalle_pedido',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_DETALLE_PEDIDO" },
        ]
      },
      {
        name: "FK_CONTIENE_PRODUCTO",
        using: "BTREE",
        fields: [
          { name: "ID_PRODUCTO" },
        ]
      },
      {
        name: "FK_TIENE_MUCHOS",
        using: "BTREE",
        fields: [
          { name: "ID_PEDIDO" },
        ]
      },
    ]
  });
};
