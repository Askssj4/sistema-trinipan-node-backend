const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
module.exports = function(sequelize, DataTypes) {
  const usuario =  sequelize.define('usuario', {
    ID_USUARIO: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_ROL: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'rol',
        key: 'ID_ROL'
      }
    },
    USERNAME: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    PASSWORD: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ACTIVO: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    FECHA_CREACION: {
      type: DataTypes.DATE,
      allowNull: true
    },
    FECHA_MODIFICACION: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'USUARIO',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_USUARIO" },
        ]
      },
      {
        name: "fk_rol_usuario",
        using: "BTREE",
        fields: [
          { name: "ID_ROL" },
        ]
      },
    ]
  });

  
  const generateHash = async(usuario_p) =>{
    // const salt = await bcrypt.genSalt(8); 
    // usuario.CONTRASENIA_USUARIO = await bcrypt.hash(usuario.CONTRASENIA_USUARIO, salt);
    // console.log("hook called");
    // console.log("la contrasenia a hashear: ", usuario.CONTRASENIA_USUARIO)
  
    //const salt = bcrypt.genSalt(10);
    usuario_p.PASSWORD = usuario_p.PASSWORD && usuario_p.PASSWORD != "" ? bcrypt.hashSync(usuario_p.PASSWORD, 12) : "";
  }

  usuario.addHook('beforeCreate',generateHash);
  //beforeCreate: generateHash
  // tbl_n_usuario.addHook('beforeUpdate',generateHash);
  usuario.beforeUpdate(
    async (usuario) => await generateHash(usuario)
  )

    
  usuario.prototype.validPassword=function(password_p) {
    let hash = bcrypt.hash(password_p, 12, function(err, hash){
      if(err){
        console.log('error', err)
      }
      else{
        // console.log("el hash: ", hash);
        return hash;
      }
    });
    // console.log( "el hash: ", hash);
    return bcrypt.compareSync(password_p, this.PASSWORD);
  } 
  
  return usuario;
};
