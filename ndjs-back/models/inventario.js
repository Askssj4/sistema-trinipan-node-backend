const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('inventario', {
    ID_INVENTARIO: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'tipo_categoria_producto_inventario',
        key: 'ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO'
      }
    },
    CANTIDAD_ACTUAL: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    NOMBRE_TIPO_INVENTARIO: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'inventario',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_INVENTARIO" },
        ]
      },
      {
        name: "FK_RELATIONSHIP_10",
        using: "BTREE",
        fields: [
          { name: "ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO" },
        ]
      },
    ]
  });
};
