const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('catalogo_productos', {
    ID_PRODUCTO: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_CATEGORIA: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'categoria_producto',
        key: 'ID_CATEGORIA'
      }
    },
    ID_TIPO_PRODUCTO: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'tipo_producto',
        key: 'ID_TIPO_PRODUCTO'
      }
    },
    ID_MEDIDA: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'unidades_de_medida',
        key: 'ID_MEDIDA'
      }
    },
    ID_PROVEEDOR: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'proveedor',
        key: 'ID_PROVEEDOR'
      }
    },
    NOMBRE_PRODUCTO: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    DESCRIPCION_PRODUCTO: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    PRECIO_VENTA: {
      type: DataTypes.DECIMAL(10,0),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'catalogo_productos',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_PRODUCTO" },
        ]
      },
      {
        name: "FK_DISTRIBUYE",
        using: "BTREE",
        fields: [
          { name: "ID_PROVEEDOR" },
        ]
      },
      {
        name: "FK_ES_MEDIDO_EN",
        using: "BTREE",
        fields: [
          { name: "ID_MEDIDA" },
        ]
      },
      {
        name: "FK_FORMA_PARTE_DE",
        using: "BTREE",
        fields: [
          { name: "ID_TIPO_PRODUCTO" },
        ]
      },
      {
        name: "FK_PERTENECE",
        using: "BTREE",
        fields: [
          { name: "ID_CATEGORIA" },
        ]
      },
    ]
  });
};
