const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('entrega', {
    ID_PROVEEDOR: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'proveedor',
        key: 'ID_PROVEEDOR'
      }
    },
    ID_PRODUCTO: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'catalogo_producto',
        key: 'ID_PRODUCTO'
      }
    },
    ID_ENTREGA: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    }
  }, {
    sequelize,
    tableName: 'ENTREGA',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_ENTREGA" },
        ]
      },
      {
        name: "FK_ENTREGA",
        using: "BTREE",
        fields: [
          { name: "ID_PRODUCTO" },
        ]
      },
      {
        name: "FK_ENTREGA2",
        using: "BTREE",
        fields: [
          { name: "ID_PROVEEDOR" },
        ]
      },
    ]
  });
};
