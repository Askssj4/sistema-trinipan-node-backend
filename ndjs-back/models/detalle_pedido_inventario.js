const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('detalle_pedido_inventario', {
    ID_DETALLE_PEDIDO_INVENTARIO: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    ID_DETALLE_PEDIDO: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'detalle_pedido',
        key: 'ID_DETALLE_PEDIDO'
      }
    },
    ID_INVENTARIO: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'inventario',
        key: 'ID_INVENTARIO'
      }
    },
    CANTIDAD_ACTUAL: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    CANTIDAD: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'detalle_pedido_inventario',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ID_DETALLE_PEDIDO_INVENTARIO" },
        ]
      },
      {
        name: "FK_RELATIONSHIP_11",
        using: "BTREE",
        fields: [
          { name: "ID_DETALLE_PEDIDO" },
        ]
      },
      {
        name: "FK_RELATIONSHIP_12",
        using: "BTREE",
        fields: [
          { name: "ID_INVENTARIO" },
        ]
      },
    ]
  });
};
