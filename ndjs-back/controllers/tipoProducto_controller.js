'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const {sequelize} = require('../models/index.js');
//importamos modelo para usuarios.
let init_models= require("../models/init-models");

var Entity= init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const tipo_producto = async(req, res) =>{
    try{
        let lista_tipo_producto = [];

        const tipos = await Entity.tipo_producto.findAll()

        if(tipos.length)
        {
            for(let tipos_it of tipos)
            {
                let id_tipo_producto = tipos_it.ID_TIPO_PRODUCTO;
                let nombre_tipo_producto = tipos_it.NOMBRE_TIPO_PRODUCTO
                let pivote = {id_tipo_producto, nombre_tipo_producto}

                lista_tipo_producto.push(pivote)
            }
        }

        res.status(200).send(lista_tipo_producto)
    }
    catch(e)
    {
        console.log(e);
        res.status(500).send({errorMessage:"Excepción del servidor.",e})
    }
}

module.exports={
    tipo_producto
}