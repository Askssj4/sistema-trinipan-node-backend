'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const {sequelize} = require('../models/index.js');
//importamos modelo para usuarios.
let init_models= require("../models/init-models");

var Entity= init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const guardar_producto = async(req, res)=>{
    try{
        let{
            id_categoria,
            id_tipo_producto,
            id_proveedor,
            id_unidad_de_medida,
            nombre_producto,
            descripcion,
            bandera_local,
            precio_venta
        } = req.body

        let categoria_seleccionada = await Entity.categoria_producto.findByPk(id_categoria)
        let tipo_producto_seleccionado = await Entity.tipo_producto.findByPk(id_tipo_producto)
        let proveedor_seleccionado = await Entity.proveedor.findByPk(id_proveedor)
        let unidad_medida_seleccionada = await Entity.unidades_de_medida.findByPk(id_unidad_de_medida)



        let nuevo_producto = await Entity.catalogo_productos.create({
            ID_CATEGORIA: categoria_seleccionada.ID_CATEGORIA,
            ID_TIPO_PRODUCTO: tipo_producto_seleccionado.ID_TIPO_PRODUCTO,
            ID_PROVEEDOR: proveedor_seleccionado.ID_PROVEEDOR,
            ID_MEDIDA: unidad_medida_seleccionada.ID_MEDIDA,
            NOMBRE_PRODUCTO: nombre_producto,
            DESCRIPCION_PRODUCTO: descripcion,
            TIPO_PRODUCTO: tipo_producto_seleccionado.NOMBRE_TIPO_PRODUCTO,
            ES_LOCAL: bandera_local,
            PRECIO_VENTA: precio_venta,

        })

        if(nuevo_producto!=null)
        {
            res.status(200).send({message:"OK"});
        }

    }
    catch(e)
    {
        console.log(e);
        res.status(500).send({errorMessage:"Excepción del servidor.",e})
    }
}

const lista_productos = async(req, res)=>{

    try{
        let lista_productos = [];

        const lista_pro = await Entity.catalogo_productos.findAll()

        if(lista_pro.length)
        {
            for(let productos_it of lista_pro)
            {
                let id_producto = productos_it.ID_PRODUCTO;
                let nombre_producto = productos_it.NOMBRE_PRODUCTO;

                let pivote = {id_producto, nombre_producto}

                lista_productos.push(pivote)
            }
        }

        res.status(200).send(lista_productos)
    }
    catch(e)
    {
        console.log(e);
        res.status(500).send({errorMessage:"Excepción del servidor.",e})
    }
}

const actualizar_estado_producto = async(req, res)=>{

    try {
        
        let{id_producto, estado_producto} = req.body

        let actualizar_estado_producto = await Entity.catalogo_productos.update({
            ACTIVO: estado_producto
        },{
            where: {
                ID_PRODUCTO: id_producto
            }
        })

        res.status(200).send({message:"OK"});


    } catch (error) {
        res.status(500).send({errorMessage: "Ha ocurrido un error en el servidor."});
    }

}

module.exports={
    guardar_producto,
    lista_productos,
    actualizar_estado_producto
}