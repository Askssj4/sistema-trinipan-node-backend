'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const {sequelize} = require('../models/index.js');
//importamos modelo para usuarios.
let init_models= require("../models/init-models");

var Entity= init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const lista_rol_panaderia = async(req, res) =>{
    try{
        let lista_roles = [];

        const roles = await Entity.rol.findAll()

        if(roles.length)
        {
            for(let rol_it of roles)
            {
                let id_rol = rol_it.ID_ROL;
                let nombre_rol = rol_it.NOMBRE_ROL
                let pivote = {id_rol, nombre_rol}

                lista_roles.push(pivote)
            }
        }

        res.status(200).send(lista_roles)
    }
    catch(e)
    {
        console.log(e);
        res.status(500).send({errorMessage:"Excepción del servidor.",e})
    }
}

module.exports={
    lista_rol_panaderia
}