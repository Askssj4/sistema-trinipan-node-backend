'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const {sequelize} = require('../models/index.js');
//importamos modelo para usuarios.
let init_models= require("../models/init-models");

var Entity= init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const unidades_de_medida = async(req, res) =>{
    try{
        let lista_unidades = [];

        const unidades_medida = await Entity.unidades_de_medida.findAll()

        if(unidades_medida.length)
        {
            for(let medida_it of unidades_medida)
            {
                let nombre_unidad = medida_it.NOMBRE_UNIDAD_MEDIDA;
                let id_unidad_medida = medida_it.ID_MEDIDA
                let pivote = {id_unidad_medida, nombre_unidad}

                lista_unidades.push(pivote)
            }
        }

        res.status(200).send(lista_unidades)
    }
    catch(e)
    {
        console.log(e);
        res.status(500).send({errorMessage:"Excepción del servidor.",e})
    }
}

module.exports={
    unidades_de_medida
}