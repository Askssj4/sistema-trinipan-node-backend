'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const {sequelize} = require('../models/index.js');
//importamos modelo para usuarios.
let init_models= require("../models/init-models");

var Entity= init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const lista_tipo_inventario = async(req, res)=>{
    try{

        let lista_tipo_inventario = []

        const tipos_inventario = await Entity.tipo_categoria_producto_inventario.findAll()

        if(tipos_inventario.length)
        {
            for(let tipos_it of tipos_inventario)
            {
                console.log("ITERATOR", tipos_it)
                let id_tipo_inventario = tipos_it.ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO;
                let nombre_tipo_inventario = tipos_it.NOMBRE_TIPO_CATEGORIA_INVENTARIO;

                let pivote = {id_tipo_inventario, nombre_tipo_inventario}

                lista_tipo_inventario.push(pivote)

            }
            console.log("LISTA TIPOS", lista_tipo_inventario)
            res.status(200).send(lista_tipo_inventario)
        }

    }
    catch(e)
    {
        console.log(e);
        res.status(500).send({errorMessage:"Excepción del servidor.",e})
    }
}

module.exports={
    lista_tipo_inventario
}