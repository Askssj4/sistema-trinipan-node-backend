'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const { sequelize } = require('../models/index.js');
//importamos modelo para usuarios.
let init_models = require("../models/init-models");

var Entity = init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const lista_productos_inventario = async (req, res) => {

    try {
        let {
            id_tipo_inventario
        } = req.body

        let inventario_seleccionado = []

        if(id_tipo_inventario==0)
        {
            inventario_seleccionado = await Entity.inventario.findAll()
        }
        else{
            inventario_seleccionado = await Entity.inventario.findAll({
                where: {
                    ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO: id_tipo_inventario
                }
            })
        }

        let lista_detalles_pedido_inventario = []
        console.log("inventario_seleccionado", inventario_seleccionado)

        for (let i of inventario_seleccionado) {
            let {
                ID_INVENTARIO,
                CANTIDAD_ACTUAL,
                ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO
            } = i

            let id_inventario = ID_INVENTARIO

            let tipo_inventario = await Entity.tipo_categoria_producto_inventario.findByPk(ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO)

            let{NOMBRE_TIPO_CATEGORIA_INVENTARIO} = tipo_inventario

            /* let detalles_pedido_inventario = await Entity.detalle_pedido_inventario.findAll({
                where:{
                    ID_INVENTARIO: id_inventario
                }
            }) */

            /* let inventario_seleccionado = await Entity.inventario.findAll({
                where: {
                    ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO: id_inventario
                }
            })


            for (let i of inventario_seleccionado) {
                console.log("i", i)

                let {ID_DETALLE_PEDIDO, CANTIDAD_ACTUAL, CANTIDAD_INGRESADAID_INVENTARIO } = i

                let id_inventario = ID_INVENTARIO */

                let detalle_pedido_inventario = await Entity.detalle_pedido_inventario.findAll({
                    where: {
                        ID_INVENTARIO: id_inventario
                    }
                })



                if (detalle_pedido_inventario != null && detalle_pedido_inventario.length > 0) {

                    for (let i of detalle_pedido_inventario) {
                        console.log("DETALLE PEDIDO INVENTARIO", i)

                        let { ID_DETALLE_PEDIDO} = i

                        let id_detalle_pedido = ID_DETALLE_PEDIDO
                        console.log("ID_DETALLE_PEDIDO", ID_DETALLE_PEDIDO)

                        let  detalles_pedido = await Entity.detalle_pedido.findByPk(id_detalle_pedido)

                        /* let detalles_pedido = await Entity.detalle_pedido.findAll({
                            where:
                            {
                                ID_DETALLE_PEDIDO: id_detalle_pedido
                            }
                        }) */

                        /* console.log("DETALLES PEDIDO", detalle_pedido); */

                        if (detalles_pedido != null) {
                            console.log("DETALLES PEDIDO", detalles_pedido);
                            let { ID_PRODUCTO, PRECIO_COMPRA, FECHA_VENCIMIENTO } = detalles_pedido

                            console.log("ID PRODUCTO", ID_PRODUCTO)

                            let datos_producto = await Entity.catalogo_productos.findByPk(ID_PRODUCTO)

                            if (datos_producto != null) {
                                let { ID_MEDIDA, NOMBRE_PRODUCTO, DESCRIPCION_PRODUCTO, TIPO_PRODUCTO, PRECIO_VENTA } = datos_producto

                                let unidades_medida = await Entity.unidades_de_medida.findByPk(ID_MEDIDA)

                                if (unidades_medida != null) {
                                    let { NOMBRE_UNIDAD_MEDIDA } = unidades_medida

                                    let NOMBRE_UNIDAD_DE_MEDIDA = NOMBRE_UNIDAD_MEDIDA
                                    let UNIDAD_VENTA = PRECIO_VENTA
                                    let COSTO_UNITARIO = PRECIO_COMPRA

                                    let label = NOMBRE_PRODUCTO + "-" + NOMBRE_TIPO_CATEGORIA_INVENTARIO
                                    let value = id_inventario

                                    let datos_detalle_pedido = {
                                        NOMBRE_PRODUCTO,
                                        DESCRIPCION_PRODUCTO,
                                        NOMBRE_UNIDAD_DE_MEDIDA,
                                        FECHA_VENCIMIENTO,
                                        COSTO_UNITARIO,
                                        UNIDAD_VENTA,
                                        CANTIDAD_ACTUAL,
                                        id_inventario,
                                        ID_PRODUCTO,
                                        NOMBRE_TIPO_CATEGORIA_INVENTARIO,
                                        label,
                                        value
                                    }

                                    lista_detalles_pedido_inventario.push(datos_detalle_pedido)
                                }
                            }

                        }
                        /* let{ID_PRODUCTO, FECHA_INGRESO, FECHA_VENCIMIENTO, COSTO_UNITARIO, UNIDAD_VENTA} = detalle_pedido

                        let id_producto = ID_PRODUCTO

                        let datos_producto = await Entity.catalogo_producto.findByPk(id_producto)

                        if(datos_producto!=null)
                        {
                            let{ID_UNIDAD_DE_MEDIDA, NOMBRE_PRODUCTO, DESCRIPCION, TIPO_PRODUCTO} = datos_producto

                            let unidades_medida = await Entity.unidades_de_medida.findByPk(ID_UNIDAD_DE_MEDIDA)

                            if(unidades_medida!=null)
                            {
                                let{NOMBRE_UNIDAD_DE_MEDIDA} = unidades_medida

                                let datos_detalle_pedido = {
                                    NOMBRE_PRODUCTO,
                                    DESCRIPCION,
                                    NOMBRE_UNIDAD_DE_MEDIDA,
                                    FECHA_INGRESO,
                                    FECHA_VENCIMIENTO,
                                    COSTO_UNITARIO,
                                    UNIDAD_VENTA,
                                    CANTIDAD_ACTUAL,
                                    CANTIDAD_INGRESADA
                                }

                                lista_detalles_pedido_inventario.push(datos_detalle_pedido)
                            }
                        } */
                    }


            }

            /* console.log("DETALLE", detalles_pedido_inventario) */
        }

        res.status(200).send(lista_detalles_pedido_inventario)
    }
    catch (e) {
        console.log(e);
        res.status(500).send({ errorMessage: "Excepción del servidor.", e })
    }


}

const ajustar_inventario = async (req, res) => {

    try {
        
        let{id_inventario, nueva_cantidad} = req.body


        let inventario = await Entity.inventario.update({
            CANTIDAD_ACTUAL: nueva_cantidad
        },{
            where:
            {ID_INVENTARIO: id_inventario}
        })

        res.status(200).send({message:"OK"});

    } catch (error) {
        res.status(500).send({errorMessage: "Ha ocurrido un error en el servidor."});
    }

}

module.exports = {
    lista_productos_inventario,
    ajustar_inventario
}