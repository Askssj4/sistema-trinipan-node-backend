'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const {sequelize} = require('../models/index.js');
//importamos modelo para usuarios.
let init_models= require("../models/init-models");

var Entity= init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const proveedor = async(req, res) =>{
    try{
        let lista_proveedores = [];

        const proveedores = await Entity.proveedor.findAll()

        if(proveedores.length)
        {
            for(let proveedor_it of proveedores)
            {
                let nombre_proveedor = proveedor_it.NOMBRE_PROVEEDOR;
                let id_proveedor = proveedor_it.ID_PROVEEDOR
                let pivote = {id_proveedor, nombre_proveedor}

                lista_proveedores.push(pivote)
            }
        }

        res.status(200).send(lista_proveedores)
    }
    catch(e)
    {
        console.log(e);
        res.status(500).send({errorMessage:"Excepción del servidor.",e})
    }
}

module.exports={
    proveedor
}