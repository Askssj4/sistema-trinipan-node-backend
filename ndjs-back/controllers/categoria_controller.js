'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const {sequelize} = require('../models/index.js');
//importamos modelo para usuarios.
let init_models= require("../models/init-models");

var Entity= init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const categorias = async(req, res) =>{
    try{
        let lista_categorias = [];

        const categorias = await Entity.categoria_producto.findAll()

        if(categorias.length)
        {
            for(let categoria_it of categorias)
            {
                let id_categoria = categoria_it.ID_CATEGORIA;
                let nombre_categoria = categoria_it.NOMBRE_CATEGORIA
                let pivote = {id_categoria, nombre_categoria}

                lista_categorias.push(pivote)
            }
        }

        res.status(200).send(lista_categorias)
    }
    catch(e)
    {
        console.log(e);
        res.status(500).send({errorMessage:"Excepción del servidor.",e})
    }
}

module.exports={
    categorias
}