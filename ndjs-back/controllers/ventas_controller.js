'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const { sequelize } = require('../models/index.js');
//importamos modelo para usuarios.
let init_models = require("../models/init-models");

var Entity = init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const _lista_productos_despacho = async (req, res) => {
    try {

        let lista_despachos = []

        let despachos = await Entity.ventas.findAll();

        console.log("DESPACHOS", despachos);

        for (let i of despachos) {
            let { ID_VENTA, FECHA_VENTA, EMPLEADO_VENTA, NUMERO_DOCUMENTO } = i

            let id_venta = ID_VENTA

            let detalle_venta = await Entity.detalle_venta.findAll({
                where: {
                    ID_VENTA: id_venta
                }
            })

            console.log("DETALLE VENTA", detalle_venta.length);

            if (detalle_venta.length > 0) {
                for (let i of detalle_venta) {
                    let {  ID_DETALLA_VENTA, ID_PRODUCTO, CANTIDAD_VENTA } = i

                    console.log("IIIII", i);

                    let nombre_producto = ""

                    let id_producto = ID_PRODUCTO

                    let producto = await Entity.catalogo_productos.findByPk(id_producto)

                    let { NOMBRE_PRODUCTO } = producto

                    /* if (producto != null) {
                        

                        nombre_producto = NOMBRE_PRODUCTO
                    } */

                    console.log("PRODUCTO", NOMBRE_PRODUCTO);

                    let id_detalle_venta = ID_DETALLA_VENTA

                    let detalle_venta_inventario = await Entity.detalle_venta_inventario.findAll({
                        where: {
                            ID_DETALLA_VENTA: id_detalle_venta
                        }
                    })
                    if (detalle_venta_inventario.length > 0) {
                        console.log("SI ENCONTRE", detalle_venta_inventario);
                        for (let i of detalle_venta_inventario) {
                            let { ID_INVENTARIO } = i

                            let id_inventario_buscado = ID_INVENTARIO

                            let inventario = await Entity.inventario.findByPk(id_inventario_buscado)

                            console.log("INV BUSCADO", typeof inventario);

                            if (inventario != null) {
                                let { ID_INVENTARIO, ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO, CANTIDAD_ACTUAL } = inventario

                                console.log("ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO", ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO);

                                let id_tipo = ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO


                                let tipo_producto = await Entity.tipo_categoria_producto_inventario.findByPk(id_tipo)

                                console.log("TIPO PRODUCTO", tipo_producto);

                                if (tipo_producto != null) {
                                    let { NOMBRE_TIPO_CATEGORIA_INVENTARIO } = tipo_producto

                                    let detalles_despacho = {
                                        ID_INVENTARIO,
                                        ID_PRODUCTO,
                                        NOMBRE_TIPO_CATEGORIA_INVENTARIO,
                                        CANTIDAD_ACTUAL,
                                        CANTIDAD_VENTA,
                                        NOMBRE_PRODUCTO,
                                        FECHA_VENTA,
                                        EMPLEADO_VENTA,
                                        NUMERO_DOCUMENTO
                                    }
                                    console.log("DETALLES DESPACHO", detalles_despacho);

                                    lista_despachos.push(detalles_despacho)
                                }
                            }
                        }
                    }
                    else{
                        console.log("NO ENCONTRË NADA");
                    }
                }
            }

        }
        res.status(200).send(lista_despachos)

    } catch (error) {
        console.log(error);
        res.status(500).send({ errorMessage: "Excepción del servidor.", e })
    }
}

const _guardar_detalle_venta = async (req, res) => {

    try {
        let{
            lista_despachos
        } = req.body

        for(let i of lista_despachos)
        {
            let{
                id_inventario,
                id_producto,
                cantidad_despacho,
                empleado_venta,
                numero_documento,
                fecha_venta,
                precio_venta,
            } = i
        
            console.log("ENTRE A GUARDAR")
    
            let inventario_buscado = await Entity.inventario.findByPk(id_inventario);
    
            console.log("INVENTARIO BUSCADO", inventario_buscado)
        
            let{CANTIDAD_ACTUAL} = inventario_buscado
    
            console.log("CANTIDAD_ACTUAL", CANTIDAD_ACTUAL)
        
            let nueva_cantidad = CANTIDAD_ACTUAL - cantidad_despacho
    
            console.log("NUEVA CANTIDAD", nueva_cantidad)
        
            let inventario_actualizado = await Entity.inventario.update({
                CANTIDAD_ACTUAL: nueva_cantidad
            },{
                where:{
                    ID_INVENTARIO: id_inventario
                }
            })
    
            console.log("INVNETARIO ACTUALIZADO", inventario_actualizado)
        
            let inventario_actualizado_cantidad = await Entity.inventario.findByPk(id_inventario);
        
            console.log("NUEVA inventario_actualizado_cantidad", inventario_actualizado_cantidad)
    
    
            let nueva_venta  = await Entity.ventas.create({
                EMPLEADO_VENTA: empleado_venta,
                NUMERO_DOCUMENTO: numero_documento,
                FECHA_VENTA: fecha_venta
            })
        
            let nuevo_detalle_venta = await Entity.detalle_venta.create({
                ID_PRODUCTO: id_producto,
                ID_VENTA: nueva_venta.ID_VENTA,
                CANTIDAD_VENTA: cantidad_despacho,
                PRECIO_VENTA: precio_venta
            })
        
            let nuevo_detalle_venta_inventario = await Entity.detalle_venta_inventario.create({
                ID_DETALLA_VENTA: nuevo_detalle_venta.ID_DETALLA_VENTA,
                ID_INVENTARIO: id_inventario,
                CANTIDAD_ACTUAL: inventario_actualizado_cantidad.CANTIDAD_ACTUAL,
                CANTIDAD_VENTA: cantidad_despacho
            })
    
        }

        res.status(200).send({message:"OK"});
    } catch (error) {
        console.log(error);
        res.status(500).send({errorMessage: "Ha ocurrido un error en el servidor."});
    }

}



module.exports = {
    _lista_productos_despacho,
    _guardar_detalle_venta
}