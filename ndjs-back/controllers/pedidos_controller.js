'use strict'
// importamos mediante require modelos y librerias necesarias.

//Conexión a BD.
const {sequelize} = require('../models/index.js');
//importamos modelo para usuarios.
let init_models= require("../models/init-models");

var Entity= init_models(sequelize); // inicialización de los modelos.
const { DateTime } = require("luxon");

//Servicios
const auth_service = require("../services/auth_service");

const guardar_pedido = async(req, res)=>{
    try{

        let{
        lista_detalles,
        empleado_recibe,
        numero_documento,
        proveedor
        } = req.body

        let proveedor_seleccionado = await Entity.proveedor.findByPk(proveedor)

        let nuevo_pedido = await Entity.pedido.create({
            EMPLEADO_RECIBE: empleado_recibe,
            NUMERO_DOCUMENTO: numero_documento,
            ID_PROVEEDOR: proveedor_seleccionado.ID_PROVEEDOR
        });

        

        for(let i of lista_detalles)
        {
            let{
                id_producto,
                fecha_ingreso, 
                fecha_vencimiento, 
                cantidad,
                costo_unitario, 
                unidad_venta,
                costo_total, 
                id_inventario,
                nombre,
                precio_compra
            } = i

            fecha_ingreso = DateTime.now();

            let producto_seleccionado = await Entity.catalogo_productos.findByPk(id_producto)

            let nuevo_inventario =  await Entity.inventario.create({
                NOMBRE_TIPO_INVENTARIO: nombre,
                ID_TIPO_CATEGORIA_PRODUCTO_INVENTARIO: id_inventario,
                EXISTENCIAS: cantidad,
                CANTIDAD_ACTUAL: cantidad,
            })

            let nuevo_detalle_pedido = await Entity.detalle_pedido.create({
                ID_PEDIDO: nuevo_pedido.ID_PEDIDO,
                ID_PRODUCTO: producto_seleccionado.ID_PRODUCTO,
                FECHA_INGRESO: fecha_ingreso,
                FECHA_VENCIMIENTO: fecha_vencimiento,
                CANTIDAD_PEDIDO: cantidad,
                PRECIO_COMPRA: precio_compra,
                COSTO_UNITARIO: costo_unitario,
                UNIDAD_VENTA: unidad_venta,
                COSTO_TOTAL: costo_total
            })
    
            let nuevo_detalle_pedido_inventario = await Entity.detalle_pedido_inventario.create({
                ID_DETALLE_PEDIDO: nuevo_detalle_pedido.ID_DETALLE_PEDIDO,
                ID_INVENTARIO: nuevo_inventario.ID_INVENTARIO,
                CANTIDAD_ACTUAL: cantidad,
                CANTIDAD_INGRESADA: cantidad,
                CANTIDAD: cantidad
            })
        }

        res.status(200).send({message:"OK"});

    }
    catch(e)
    {
        console.log(e);
        res.status(500).send({errorMessage:"Excepción del servidor.",e})
    }
}

module.exports={
    guardar_pedido,
}