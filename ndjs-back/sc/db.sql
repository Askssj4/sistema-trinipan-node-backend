CREATE DATABASE bd_panaderia_trinipan;

drop table if exists CATALOGO_PRODUCTO;

drop table if exists CATEGORIA;

drop table if exists DETALLE_PEDIDO;

drop table if exists DETALLE_PEDIDO_INVENTARIO;

drop table if exists ENTREGA;

drop table if exists INVENTARIO;

drop table if exists PEDIDO;

drop table if exists PROVEEDOR;

drop table if exists TIPO_INVENTARIO;

drop table if exists TIPO_PRODUCTO;

drop table if exists TIPO_PRODUCTO_INVENTARIO;

/*==============================================================*/
/* Table: CATALOGO_PRODUCTO                                     */
/*==============================================================*/
create table CATALOGO_PRODUCTO
(
   ID_PRODUCTO          INT NOT NULL AUTO_INCREMENT,
   ID_CATEGORIA         INT not null,
   ID_TIPO_PRODUCTO     INT not null,
   ID_PROVEEDOR		INT not null,
   ID_UNIDAD_DE_MEDIDA  INT not null,
   NOMBRE_PRODUCTO      varchar(50) not null,
   TIPO_PRODUCTO        varchar(20) not null,
   DESCRIPCION          varchar(50) not null,
   ES_LOCAL             boolean not null,
   primary key (ID_PRODUCTO)
);

/*==============================================================*/
/* Table: CATEGORIA                                             */
/*==============================================================*/
create table CATEGORIA
(
   ID_CATEGORIA         INT NOT NULL AUTO_INCREMENT,
   ID_TIPO_PRODUCTO              INT not null,
   primary key (ID_CATEGORIA)
);

/*==============================================================*/
/* Table: DETALLE_PEDIDO                                        */
/*==============================================================*/
create table DETALLE_PEDIDO
(
   ID_DETALLE_PEDIDO      INT NOT NULL AUTO_INCREMENT,
   ID_PEDIDO              INT not null,
   ID_PRODUCTO          INT not null,
   FECHA_INGRESO        date not null,
   FECHA_VENCIMIENTO    date not null,
   CANTIDAD             int not null,
   COSTO_UNITARIO       decimal not null,
   UNIDAD_VENTA         char(10) not null,
   COSTO_TOTAL          decimal not null,
   primary key (ID_DETALLE_PEDIDO)
);

/*==============================================================*/
/* Table: DETALLE_PEDIDO_INVENTARIO                             */
/*==============================================================*/
create table DETALLE_PEDIDO_INVENTARIO
(
   ID_DETALLE_PEDIDO_INVENTARIO INT NOT NULL AUTO_INCREMENT,
   ID_INVENTARIO        INT not null,
   ID_DETALLE_PEDIDO    INT not null,
   CANTIDAD_ACTUAL      int not null,
   CANTIDAD_INGRESADA   int not null,
   primary key (ID_DETALLE_PEDIDO_INVENTARIO)
);

/*==============================================================*/
/* Table: ENTREGA                                               */
/*==============================================================*/
create table ENTREGA
(
   ID_PROVEEDOR         int not null,
   ID_PRODUCTO          int not null,
   ID_ENTREGA		INT NOT NULL AUTO_INCREMENT,
   primary key (ID_ENTREGA)
);

/*==============================================================*/
/* Table: INVENTARIO                                            */
/*==============================================================*/
create table INVENTARIO
(
   ID_INVENTARIO        INT NOT NULL AUTO_INCREMENT,
   NOMBRE_INVENTARIO	varchar(50) not null,
   ID_TIPO_INVENTARIO   int not null,
   EXISTENCIAS          int not null,
   primary key (ID_INVENTARIO)
);

/*==============================================================*/
/* Table: PEDIDO                                                */
/*==============================================================*/
create table PEDIDO
(
   ID_PEDIDO              INT NOT NULL AUTO_INCREMENT,
   primary key (ID_PEDIDO)
);

/*==============================================================*/
/* Table: PROVEEDOR                                             */
/*==============================================================*/
create table PROVEEDOR
(
   ID_PROVEEDOR         INT NOT NULL AUTO_INCREMENT,
   NOMBRE_PROVEEDOR     varchar(50) not null,
   TELEFONO             varchar(9) not null,
   CORREO               varchar(50) not null,
   primary key (ID_PROVEEDOR)
);

/*==============================================================*/
/* Table: TIPO_INVENTARIO                                       */
/*==============================================================*/
create table TIPO_INVENTARIO
(
   ID_TIPO_INVENTARIO              INT NOT NULL AUTO_INCREMENT,
   TIPO_INVENTARIO      varchar(30) not null,
   primary key (ID_TIPO_INVENTARIO)
);

/*==============================================================*/
/* Table: TIPO_PRODUCTO                                         */
/*==============================================================*/
create table TIPO_PRODUCTO
(
   ID_TIPO_PRODUCTO              INT NOT NULL AUTO_INCREMENT,
   NOMBRE_TIPO_PRODUCTO		 VARCHAR(30) not NULL,
   primary key (ID_TIPO_PRODUCTO)
);

/*==============================================================*/
/* Table: TIPO_PRODUCTO_INVENTARIO                              */
/*==============================================================*/
create table TIPO_PRODUCTO_INVENTARIO
(
   ID_TIPO_PRODUCTO              int not null,
   ID_INVENTARIO        int not null,
   ID_TIPO_PRODUCTO_INVENTARIO INT NOT NULL AUTO_INCREMENT,
   ACTIVO               boolean not null,
   primary key (ID_TIPO_PRODUCTO_INVENTARIO)
);

/*==============================================================*/
/* Table: UNIDAD_DE_MEDIDA                             */
/*==============================================================*/
create table UNIDAD_DE_MEDIDA
(
    ID_UNIDAD_DE_MEDIDA INT NOT NULL AUTO_INCREMENT,
    NOMBRE_UNIDAD_DE_MEDIDA VARCHAR(30) NULL,
    PRIMARY KEY (ID_UNIDAD_DE_MEDIDA)
);

alter table CATALOGO_PRODUCTO add constraint FK_AGRUPA foreign key (ID_CATEGORIA)
      references CATEGORIA (ID_CATEGORIA) on delete restrict on update restrict;

alter table CATALOGO_PRODUCTO add constraint FK_PERTENECE foreign key (ID_TIPO_PRODUCTO)
      references TIPO_PRODUCTO (ID_TIPO_PRODUCTO) on delete restrict on update restrict;

alter table CATEGORIA add constraint FK_ORDENA foreign key (ID_TIPO_PRODUCTO)
      references TIPO_PRODUCTO (ID_TIPO_PRODUCTO) on delete restrict on update restrict;

alter table CATALOGO_PRODUCTO add constraint FK_TIENE foreign key (ID_UNIDAD_DE_MEDIDA)
      references UNIDAD_DE_MEDIDA (ID_UNIDAD_DE_MEDIDA) on delete restrict on update restrict;

alter table CATALOGO_PRODUCTO add constraint FK_ABASTECE foreign key (ID_PROVEEDOR)
      references PROVEEDOR (ID_PROVEEDOR) on delete restrict on update restrict;

alter table DETALLE_PEDIDO add constraint FK_CONTIENE foreign key (ID_PEDIDO)
      references PEDIDO (ID_PEDIDO) on delete restrict on update restrict;

alter table DETALLE_PEDIDO add constraint FK_RELACIONA foreign key (ID_PRODUCTO)
      references CATALOGO_PRODUCTO (ID_PRODUCTO) on delete restrict on update restrict;

alter table DETALLE_PEDIDO_INVENTARIO add constraint FK_CONTIENE_2 foreign key (ID_INVENTARIO)
      references INVENTARIO (ID_INVENTARIO) on delete restrict on update restrict;

alter table DETALLE_PEDIDO_INVENTARIO add constraint FK_CONTIENE_3 foreign key (ID_DETALLE_PEDIDO)
      references DETALLE_PEDIDO (ID_DETALLE_PEDIDO) on delete restrict on update restrict;

alter table ENTREGA add constraint FK_ENTREGA foreign key (ID_PRODUCTO)
      references CATALOGO_PRODUCTO (ID_PRODUCTO) on delete restrict on update restrict;

alter table ENTREGA add constraint FK_ENTREGA2 foreign key (ID_PROVEEDOR)
      references PROVEEDOR (ID_PROVEEDOR) on delete restrict on update restrict;

alter table INVENTARIO add constraint FK_ES_PARTE foreign key (ID_TIPO_INVENTARIO)
      references TIPO_INVENTARIO (ID_TIPO_INVENTARIO) on delete restrict on update restrict;

alter table TIPO_PRODUCTO_INVENTARIO add constraint FK_RELACIONA_2 foreign key (ID_TIPO_PRODUCTO)
      references TIPO_PRODUCTO (ID_TIPO_PRODUCTO) on delete restrict on update restrict;

alter table TIPO_PRODUCTO_INVENTARIO add constraint FK_RELACIONA_3 foreign key (ID_INVENTARIO)
      references INVENTARIO (ID_INVENTARIO) on delete restrict on update restrict;
