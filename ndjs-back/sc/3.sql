drop table if exists USUARIOS;

/*==============================================================*/
/* Table: USUARIOS                                              */
/*==============================================================*/
create table USUARIOS
(
   ID_USUARIO           int not null auto_increment,
   NOMBRE_USUARIO       varchar(30),
   CONTRASENA           varchar(100),
   primary key (ID_USUARIO)
);

