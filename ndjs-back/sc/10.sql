drop table if exists ROL;

/*==============================================================*/
/* Table: ROL                                              */
/*==============================================================*/
create table ROL
(
   ID_ROL           int not null auto_increment,
   NOMBRE_ROL      	varchar(30),
   primary key (ID_ROL)
);